# Available in buster-backports
Django>=2.0,<3.0

# Available in Debian
PyYAML
django-countries
psycopg2
python-memcached

# From the cheeseshop
bleach
bleach-allowlist
django-crispy-forms>=1.7.0
mdx_linkify==2.1
mdx_staticfiles==0.1
stripe==2.60.0
wafer==0.11.0
wafer-debconf==0.6.8

# unused but a dependency of django-bakery
# pinned to avoid having to update it every day
boto3==1.18.44
botocore==1.21.44
