---
name: The Conference Venue
---
# Conference Venue

## The Park

<img class="img-fluid" src="{% static "img/itpvenue.jpg" %}"
     title="ITP Venue">

The conference will be held at [Innovation & Training
Park](https://itp-prizren.com/).

Innovation & Training Park (ITP) Prizren is a business park located in a
suburb of the city of Prizren.
The area of 39 hectares offers a networking atmosphere that builds on
collaboration and shared resources.
ITP benefits traditional economic sectors by acting as a center for the
distribution of innovative technologies.

The buildings in the area include 23 accommodation houses,
administrative buildings, maintenance buildings, workshop sheds, a
sports facility, recreational facilities such as cafes and a modern
canteen.



<img class="img-fluid" src="{% static "img/itpvenue2.jpg" %}"
     title="ITP Venue from above">

## Address

Innovation and Training Park (ITP)  
Ukë Bytyçi St.  
20000 Prizren, Kosovo

[Map](https://www.openstreetmap.org/#map=16/42.2211/20.7487)


## Rooms

 * Drini - Main conference hall
 * Lumbardhi - Red building conference hall 
 * Ereniku - Second red building conference hall
 * Mirusha - Workshop room in the red building
 * Front Desk
 * Noisy hacklab
 * Quiet hacklab


## Getting to Kosovo and Prizren

All international flights to Kosovo arrive at the Prishtina
International Airport.

Kosovo has border points with Albania, North Macedonia and Montenegro,
which may be used to enter the country.
## Getting to the venue from the Airport
### Bus

#### From Prishtina International Airport

There is a [Public transport](http://www.limakkosovo.aero/public-transport)
bus line from Prishtina International Airport to the Bus Station in Prishtina.

Take the hourly airport shuttle bus (Line 1A) to Prishtina Bus Station (3 EUR). 

Bus lines from Prishtina Bus Station to Prizren travel every 20 minutes (4 EUR). The bus passes by the venue (ask to be dropped at Kampi Gjerman i KFOR-it).

If you end up at Prizren Bus Station or any other place around Prizren, branded taxis to the venue cost 2-2.5 EUR.

Other than Prishtina International Airport, for cost considerations, you might want to use 3 other airports in the region which offer better priced airline tickets from some destinacions and especially if you are doing some tourism before or after arriving in Kosovo.

#### From Tirana International Airport

[Tirana International Airport](https://www.tirana-airport.com/) is 2:10 hrs by bus (163 km) away. Tirana - Prishtina bus stops by Tirana International Airport with a stopover in the periphery of Prizren, from which you may call a taxi.
[Bus schedule](https://gjirafa.com/Autobus?nisja=N%C3%ABn%C3%AB+Tereza%28Aeroporti%29+-+Tiran%C3%AB&destinacioni=Prishtin%C3%AB&). 
It costs 15 EUR one way. This airport is better connected to Italy and Wizz Air has more destinations. 

#### From Skopje International Airport

[Skopje International Airport](http://skp.airports.com.mk/) might also have better deals through Wizz Air and alternative destinations from those in Prishtina. 

Your route will be: Airport shuttle to Skopje Bus Station (300 denar or 3 EUR), from there direct buses to Prizren are a few. Skopje - Prishtina (7 EUR, [schedule](https://gjirafa.com/Autobus?nisja=Shkup&destinacioni=Prishtine)) is more readily available and from there to Prizren every 20 minutes (4 EUR). It will take 3-4 hrs all if best connected.


#### From Kukes International Airport

[Kukës International Airport](https://www.kuiport.al/) is a newly opened airport, in northern Albania, across the border from Prizren, with a handful of lines, currently only serving Switzerland, mostly through charter lines. Get to the town of Kukës, from there vans (furgon) are regular to Prizren. 

[Gjirafa Autobus](https://gjirafa.com/autobus) is helpful if you are getting around by bus.


For COVID-19 regulations of neighbouring countries, see: [travel through neighbouring countries](/about/travel-through-neighbours/).
### Taxi schedule
Taxi from Prishtina International Airport to Prizren costs 50 Euro.
Taxi from Prishtina International Airport to Prishtina costs 15 Euro.

## Taxi within Prizren

Taxi companies work 24/7. Taxis are quite inexpensive: you can get around the city for around €3.5. Starting price is €2.  
Some handy phrases: "Te Kampi Gjerman" is ITP. "Qender" is the center.  

Taxi companies serving Prizren:

__Radio Taxi Fortuna__  
+383 49 380 000  
+383 44 380 000  
Viber/Whatsapp  

__Taxi OK__  
+383 49 333 500  
+383 44 333 500  
Viber/Whatsapp  

__Blue Taxi__  
+383 49 555 252  
+383 44 711 711  
Viber
