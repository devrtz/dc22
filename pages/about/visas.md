---
name: Visas
---

# Visas


## Countries that can enter without visa

The exemption from the visa requirement applies to the citizens of the following countries:

 * Albania
 * Andorra
 * Antigua and Barbuda
 * Argentina
 * Australia
 * Austria[^eu]
 * Bahamas
 * Bahrain
 * Barbados
 * Belgium[^eu]
 * Belize
 * Botswana
 * Brazil
 * Brunei Darussalam
 * Bulgaria[^eu]
 * Canada
 * Chile
 * Colombia
 * Costa Rica
 * Croatia[^eu]
 * Cyprus[^eu]
 * Czech Republic[^eu]
 * Denmark[^eu]
 * Dominica
 * East Timor
 * El Salvador
 * Estonia[^eu]
 * Fiji
 * Finland[^eu]
 * France[^eu]
 * Germany[^eu]
 * Greece[^eu]
 * Grenada
 * Guatemala
 * Guiana
 * Holly See[^eu]
 * Honduras
 * Hungary[^eu]
 * Iceland[^eu]
 * Israel
 * Ireland[^eu]
 * Italy[^eu]
 * Japan
 * Jordan
 * Kingdom of Saudi Arabia
 * Kiribati
 * Kuwait
 * Latvia[^eu]
 * Lesotho
 * Liechtenstein[^eu]
 * Lithuania[^eu]
 * Luxembourg
 * Macedonia
 * Malawi
 * Malaysia
 * Maldives
 * Malta[^eu]
 * Marshall Islands
 * Mauritius
 * Mexico
 * Micronesia
 * Monaco[^eu]
 * Montenegro
 * Namibia
 * Nauru
 * Netherlands[^eu]
 * New Papua Guinea
 * New Zealand
 * Nicaragua
 * Norway[^eu]
 * Oman
 * Palau
 * Panama
 * Paraguay
 * Poland[^eu]
 * Portugal[^eu]
 * Qatar
 * Romania[^eu]
 * Saint Kitts and Nevis
 * Saint Lucia
 * Saint Vincent and Grenadine
 * Samoa
 * San Marino[^eu]
 * Sao Tome and Principe
 * Serbia
 * Seychelles Islands
 * Slovakia[^eu]
 * Slovenia[^eu]
 * Solomon Islands
 * South Africa
 * South Korea
 * Spain[^eu]
 * Swaziland
 * Sweden[^eu]
 * Swiss Confederation[^eu]
 * Tonga
 * Trinidad and Tobago
 * Turkey
 * Tuvalu
 * United Arab Emirates
 * United Kingdom and Northern Ireland
 * United States of America
 * Uruguay
 * Vanuatu
 * Venezuela

[^eu]: EU or Schengen member states

Information is taken from [The Ministry of Foreign Affairs](https://www.mfa-ks.net/)

## Special Categories Exempted from Visa Requirements

The exemption from the visa requirement applies also to the following categories:

- Citizens of the countries which are required to obtain a visa for Kosovo but hold a biometric valid residence permit issued by one of the Schengen member states or a valid multi-entry Schengen Visa are exempt from the requirement to obtain a Visa to enter, transit, or stay in the territory of the Republic of Kosovo for up to 15 days.

- Citizens of: EU and Schengen Zone Member States; The United Kingdom of Great Britain and Northern Ireland; Holy See; Principality of Andorra; Principality of Monaco; Republic of San Marino, Republic of Albania, Montenegro, and Republic of Serbia are allowed to enter, transit, and stay in Kosovo for up to 90 days for a six-months period with a valid biometric identification card.

- Holders of diplomatic and service passports issued by Russian Federation States, People’s Republic of China, Egypt, Indonesia and Ukraine shall be allowed to enter, transit or stay up to 15 days in the territory of the Republic of Kosovo.

- Holders of valid travel documents issued by Special Administrative Regions of People’s Republic of China: Hong Kong and Macao are exempted from the obligation to obtain a visa.

- Holders of travel documents issued by Taiwan shall be exempted from the obligation to obtain a visa provided that they preliminarily notify the Diplomatic or Consular Mission of the Republic of Kosovo, at least 2 weeks in advance.

- Holders of Travel documents issued by EU Member States, Schengen zone States, The United Kingdom of Great Britain and Northern Ireland, United States of America, Canada, Australia and Japan based on the 1951 Convention on Refugee Status or the 1954 Convention on the Status of Stateless Persons, as well as holders of valid travel documents for foreigners, may enter, pass through the territory and stay in the Republic of Kosovo up to 15 days without a visa.

- Holders of Laissez-Passer, regardless of their nationality, issued by United Nations Organizations, NATO, OSCE, Council of Europe and European Union, are also exempt from the visa requirement.

## Visa process for those not included above

If you are not a citizen of the countries listed above or a part of Special Categories, then you are required to apply for a visa.

Approved DebConf22 participants can apply remotely for a visa in the Consulate General of the Republic of Kosovo in Istanbul, New York or Tirana.

The procedure is as follows:

- Until May 1, all participants wanting to apply for a Kosovo visa in distance should send the first 2 pages (bio details) of their scanned passport to [visa@debconf.org][] and tell us whether they would like to apply in **Istanbul** or **New York**.  
  **Do this as soon as possible, even if you are not sure you will apply for the visa afterwards!**
- The DebConf local team will then send the list of potential applicants to the Ministry of Foreign Affairs of Kosovo to get permission for you to make a remote visa application;
- Once we get approval from MFA, we will notify you to apply via email following the procedure described below. We will also provide supporting documents from the local organization, [FLOSSK][].
- The Visa decision usually takes 15 days from the date of the application.
- After approval of the visa, you have to options:

1. The Consulate will instruct you to send your passport via post (along with any other documents they may deem necessary) for placement of the visa sticker. They will return it back to you by post.

2. One other option after this procedure, if the participants are legally unable to send their passports by mail to a Kosovo consulate outside their country, physical presence at certain Kosovo consulates to **receive** the visa is necessary.  
In this case, once your visa is approved through the remote application, visa stamping will be on walk in.   
Starting on April 20th, neighboring Albania allows visa free entrance for citizens of India, Saudi Arabia, Bahrain, Qatar, Oman, and Thailand.  
Considering this, we suggest that you fly into Tirana International Airport and have the Kosovo visa stamped at the Kosovo Embassy in Tirana, instead of doing the same in Istanbul.  
If you choose the Tirana option, by May 1st please update us of your choice in follow up to your earlier email to us, or when you initially send the passport as advised previously.  
If you choose Istanbul or New York for the second option as well, then please make sure you let the Embassy know that you will pick up the passport by 
yourself.

[visa@debconf.org]: mailto:visa@debconf.org
[FLOSSK]: https://flossk.org/

When applying remotely for a visa, you will need to provide:

- The filled and signed application form - [download here](https://storm.debian.net/shared/hmxKPEQMjKq597DkE4uBQBLxs0vVRyK0lmsO2db1IAr). If the applicant is minor, the application form must be signed by parents or legal custodian. Persons included in the applicant’s travel document shall submit a separate application form;
- Scanned passport (Valid at least three months after the visa expiration date with at least two empty pages). You will have to send scans of the first 2 pages (photo and bio page) and 2 empty pages where the visa would be placed; The Consulate might require you to scan the whole passport.
- 2 photos (35x45mm) not older than 1 month;
- Proof showing the purpose of visiting the Republic of Kosovo. We will supply a notarized invitation letter once the MFA approves our request for your remote application;
- Ticket reservation;
- Documents in relation to accommodation, or proof of sufficient means to cover their accommodation; Your Hotel reservation or a notarized letter of guarantee from the host in Kosovo - FLOSSK will state your provided accommodation at Innovation and Training Camp in Prizren;
- Sufficient financial means to cover the expenses of stay in Kosovo (Bank account statement for the last three months); documents indicating that the applicant possesses sufficient means of subsistence both for the duration of the intended stay and for the return to his country of origin or residence, or for the transit to a third country into which he is certain to be admitted, or that he is in a position to acquire such means lawfully;
- Health insurance valid throughout the territory of the Republic of Kosovo which covers the entire period of the person’s intended stay or transit.
- Proof of the visa fee payment of 40 EUR that applicants need to pay unless they are holders of diplomatic and official passports or children under six (6) years old;  
  See the bank account details below for the respective Consulate you are applying to.

Please note: despite the online application, the Consulate may require you to send original documents by mail in addition to your passport.


#### For the Consulate General of the Republic of Kosovo in Tirana:

General Consulate of the Republic of Kosovo in Tirana  
Str. Donika Kastrioti, Vila Nr. 6a, Tiranë/Shqipëri  
e-mail: consulategeneral.tir@rks-gov.net  

The visa fee can be paid by bank transfer / SWIFT.

**Name of Bank:**    Raiffeisen BANK  
**Account holder**   AMBASADA E REPUBLIKES SE KOSOVES NE K82310452S  
**IBAN:**            ``AL15202110370000004300976821``  
**SWIFT code:**      ``SGSBALTX``  
**Reference:**       The visa applicant's name.  

#### For the Consulate General of the Republic of Kosovo in Istanbul:

Consulate General of the Republic of Kosovo in Istanbul  
Vali Konaya Cad. No.: 74 D 3  
Nisantasi, Istanbul / Turkey  
**e-mail:** [visa.ist@rks-gov.net][]

[visa.ist@rks-gov.net]: mailto:visa.ist@rks-gov.net

The visa fee can be paid by bank transfer / SWIFT.

**Name of Bank:**    AKBANK  
**Address of Bank:** Teşvikiye Cad. No: 61/B, Nişantaşı Şubesi  
**IBAN:**            ``TR 31 0004 6000 7503 6000 1383 87``  
**Institution:**     Kosova Cumhuriyeti Istanbul Başkonsolosluğu  
**SWIFT code:**      ``AKBKTRIS``  
**Reference:**       The visa applicant's name.

#### For the Consulate General of the Republic of Kosovo in New York:

Consulate General of the Republic of Kosovo in New York  
801 Second Avenue, Suite 301, New York, NY 10017  
**Phone:**  +1 212 949 1400  
**Fax:**    +1 212 949 1403  
**e-mail:** [consulategeneral.ny@rks-gov.net][]

[consulategeneral.ny@rks-gov.net]: mailto:consulategeneral.ny@rks-gov.net

All payments for consular services are made through banking services. Fees for specific services must be paid to the account of the Consulate General of the Republic of Kosovo in New York, by depositing the assigned amount to the relevant bank. Payment can be made in dollars, equivalent to the amount in Euros, as exchanged by the US banks.
The Consular bank account details are as follows:

**Bank name**:      Bank of America  
**Account name**:   Consulate General of the Republic of Kosovo  
**Account number**: ``226004439707``  
**Routing Number**: ``026009593``  
**SWIFT code**:     ``BOFAUS3N``  
**Bank Address**:   675 3rd Ave, New York, NY 10017  
**Reference:**      The visa applicant's name.

#### Important Information

Depending on the purpose of visiting Kosovo the Consular Officer may require additional supporting documents;  
All supporting documents should be translated in one of the official languages of Republic of Kosovo (Albanian, Serbian or English);

The applicant whom the visa is refused can file a complaint within 8 calendar days to the respective consular mission;  
Only possessing a valid visa does not automatically guarantee entry in Kosovo.  
Border Police may refuse the entrance of the foreign national in case of reasonable suspicions regarding their documentation or reasons for entering the Republic of Kosovo.

## Additional links

[Who needs visas to Kosovo](https://www.mfa-ks.net/en/sherbimet_konsullore/503/kush-ka-nevoj-pr-viza-t-kosovs/503_)

[Special Categories Exempted from Visa Requirements](https://www.mfa-ks.net/en/sherbimet_konsullore/505/kategorit-e-veanta-t-liruara-nga-viza/505)

[Visa policy of Kosovo - Wikipedia](https://en.wikipedia.org/wiki/Visa_policy_of_Kosovo)
