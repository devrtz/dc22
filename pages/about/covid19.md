---
name: COVID-19
---

# COVID-19 Information (as of March 2022)

<br>

## Entry requirements
Every person who enters the Republic of Kosovo shall possess one of the
following evidence:

1. Complete COVID-19 vaccination with the most recent dose within the
   last 12 months,
2. A COVID-19 booster-shot after complete vaccination,
3. Recovery from COVID-19, within 90 days, with PCR test proof, or
4. A negative PCR test taken less than 48 hours before arrival.


## General Restrictions

Wearing a mask covering the nose and mouth is mandatory in closed areas.

Indoor gatherings of up to 50% of capacities (workshops, meetings,
seminars, trainings or other gatherings) are allowed.
In order to be allowed to enter these premises, a person must provide:

1. Certificate of vaccination with at least two doses or one dose of
   Janssen vaccine against COVID-19;
2. Persons with medical evidence issued by the specialist doctor of the
   respective field verifying that they have contraindications and are
   exempt from vaccination must present the negative RT-PCR test for
   COVID-19, which should not be older than 48 hours;
3. Single dose vaccination certificate, not older than one (1) month.
4. Evidence that the person has recovered from COVID-19 in the last 90
   days (positive RT-PCR test issued in the last 21–90 days).

## Conference Policy

The DebConf22 organizers are enforcing the vaccination and testing
requirements above.

If you are not vaccinated, you will need to take a COVID-19 test every
48hrs.

## COVID-19 Testing

Regarding COVID-19 testing, there are PCR and rapid antigen tests
available from several private labs in Prizren, Prishtina and other
major towns in Kosovo.
The main public hospital in Prishtina, the University Clinical Center of
Kosovo (UCCK), provides free travel testing without a referral although
availability and schedule depend on demand.

The local team will make sure to provide some rapid antigen tests,
available at the front desk, if someone needs to be tested during the
conference.

If you are in Kosovo and have COVID-19 related questions, are
symptomatic, or need information regarding testing, please call for free
038 200 80 800, or dial 194.

## Neighbouring countries COVID-19 information

Please see: [travel through neighbouring countries](/about/travel-through-neighbours/).
