---
name: Cheese and Wine Party
---
# Cheese and Wine Party

Cheese is good for you. Wine also helps.
This will be the 15th official DebConf Cheese and Wine party.
The 0th party was improvised in Helsinki during DebConf 5, in the
so-called "French" room.
It then found its way into the official timetable at DebConf 6, in
Mexico, and has been a DebConf tradition ever since.

The concept is very simple: bring tasty things from your country,
preferably edible.
Grapes and lactose are both optional: we like cheese and wine, but we
love the surprising things that people bring from all around the world.
Even if you do not bring anything, feel free to participate: our
priorities are our users and free cheese.

Some bits from previous editions:

 * [C&W13, DebConf17, Montreal, Canada, August 7th 2017](https://www.flickr.com/photos/aigarius/36398984276/in/photostream/)
 * [C&W14, DebConf18, Hsinchi, Taiwan, August 30th  2018](https://photos.google.com/share/AF1QipOroi0hV38ezF2Oa_5CmtpIvdam2rLJ2Dy6oIyzGD6AJ_Emfrrh573YxmyVoS-vUQ/photo/AF1QipNQYvb8ViztpXfkg3neI4eV0HsMC5Yt7GSayFA7?key=UEZ5VVphTlJhS2xTTl9vVjdxM0ZmbWlFcC1rTHBB)
 * [DebConf19, Curitiba, Brazil, July 22nd  2019](https://photos.google.com/share/AF1QipPekZe8o40CK9YpAu3zXcmFQW28WNMfk_x9R86I9vdFQjhwsIpyatG9f7WN2GXvCw/photo/AF1QipMYMtiBzRDPsSxmVaUXgahD-MBrYGN_Jyfu7g3C?key=b0lJUXVmLUJ2MjJQYTJ4c1NZdmtQT0hxT2JSZlBR)
 * [DebConf20, Online, August 28th, 2020](https://debconf20.debconf.org/talks/69-cheese-and-wine-at-home/)

## When?

FIXME

## Where?

In the restaurant inside the Park's facilities.

## FAQ

Can I participate in the party (i.e., eat and drink) if I haven't brought
anything?

 * Sure, you can. We do not require you to bring anything to share.
 * However, the greater the cheesy variety, the better the party will be.
 * If it is not possible to bring cheese or wine, you can buy some locally in
Haifa (see below), or you can bring some other food or drink typical of your
region.

I couldn't bring anything from home but would like to participate. Is there a way to do so?

 * You can buy something from supermarkets near from the venue.
 * You can help us too! C&W helpers meetings will be announced on [debconf-announce](https://lists.debian.org/debconf-announce/).

I brought XXX and do not know where to store it!

 * There will be a fridge on-site.

I forgot what I brought!

 * Please make sure your C&W submissions are listed in the wiki, and labelled, so that this does not happen.

Is there anything special I should do when bringing my C&W submissions?

 * Please make sure all items are clearly identified. If the packaging does not
say what you've brought, please label it yourself. The label should say what it
is, where it comes from, and (if relevant) what it is made of.
 * If your item unexpectedly contains allergens or things people may wish to
avoid (e.g. shellfish, pork, alcohol), please point it out.

Vegans do not eat cheese. Can they still attend?

 * Yes! We accept (and encourage) the submission of vegan friendly food
(including vegan cheese).

I do not drink alcohol. Can I still attend?

 * Yes! We will have water, sodas and juices too.

## Kosovan customs regulations

* FIXME!

## Would you like to help us?

There will be a volunteer sign-up during the conference.

We will need volunteers to receive and store cheese donations from
attendees, during DebCamp and DebConf.

And on the day of the party, we will need a team to prepare venue and
cheese, and clean up afterwards.
